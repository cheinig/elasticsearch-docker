Create a new Docker Image for elasticsearch

Push to repository will trigger automatic build process.

This image is only useful within a kubernetes cluster where you give this image additional permissions:  

```yaml
    spec:
      containers:
      - name: elastic-node
        image: registry.gitlab.com/cheinig/elasticsearch-docker:master
        securityContext:
          capabilities:
            add:
              - IPC_LOCK
              - SYS_RESOURCE
```